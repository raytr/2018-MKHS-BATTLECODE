/**
 * Created by raytr on 1/9/2018.
 */
import bc.*;

class StructureFactory extends StructureBase {
    StructureFactory(GameController gc){
        super(gc);
    }

    void loop(){

        //spawn rangers meta meta meta
        if(thisUnit.structureGarrison().size()>0){
            for(Direction direction : Util.DIRECTIONS){
                if (gc.canUnload(thisUnit.id(),direction)){
                    gc.unload(thisUnit.id(),direction);
                    break;
                }
            }
        }else if (Player.getNumWorkers() < 4 ){
            //All workers dead; need more
            if (gc.canProduceRobot(thisUnit.id(),UnitType.Worker)){
                gc.produceRobot(thisUnit.id(),UnitType.Worker);
            }
        } else if (gc.canProduceRobot(thisUnit.id(),UnitType.Ranger)){
            int random = Util.randIntInclusive(0,10);
            if (random < 4 && Player.getNumRangers() > 5){
                gc.produceRobot(thisUnit.id(),UnitType.Healer);
            } else{
                gc.produceRobot(thisUnit.id(),UnitType.Ranger);
            }
        }



    }
}
