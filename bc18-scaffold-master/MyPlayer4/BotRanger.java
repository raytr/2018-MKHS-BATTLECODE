/**
 * Created by raytr on 1/9/2018.
 */
import bc.*;

class BotRanger extends BotBase{
    BotRanger(GameController gc){
        super(gc);
    }
    void loop(){
        //go towards bad guys
        //make sure we're not in the factory garrison

        if(thisUnit.location().isOnMap()){
            //enemies = gc.senseNearbyUnitsByTeam(thisUnit.location().mapLocation(), thisUnit.visionRange(), Player.enemyTeam);
            //go to closest enemy

            Unit closestEnemy = getClosestEnemyInMapVision();

            /** ATTACKING **/
            if (gc.isAttackReady(thisUnit.id())) {
                VecUnit enemies = gc.senseNearbyUnitsByTeam(thisUnit.location().mapLocation(), thisUnit.attackRange(), GameConstants.enemyTeam);
                if (enemies.size() > 0) {
                    //for some reason they stopped shooting suddenly on mars
                    for (int i = 0; i < enemies.size(); i++) {
                        if (gc.canAttack(thisUnit.id(), enemies.get(i).id())) {
                            gc.attack(thisUnit.id(), enemies.get(i).id());
                            break;
                        }
                    }
                } else if (numTurnsAlive % 3 == 0 && closestEnemy != null && gc.canBeginSnipe(thisUnit.id(), closestEnemy.location().mapLocation()) && gc.isBeginSnipeReady(thisUnit.id())) {
                    gc.beginSnipe(thisUnit.id(), closestEnemy.location().mapLocation());
                }
            }
            /** MOVING **/
            if (!completelyTrapped() && gc.isMoveReady(thisUnit.id())) {
                if (thisUnit.location().isOnPlanet(Planet.Earth)) {
                    if (!boardNearbyRockets()) {

                        if (closestEnemy != null && gc.getTimeLeftMs() > 5000) {
                            if (closestEnemy.location().mapLocation().distanceSquaredTo(thisUnit.location().mapLocation()) <= 10)
                                return;
                            //System.out.println("EY KILLEM");
                            setTargetLoc(closestEnemy.location().mapLocation());
                            takeStepToTarget();
                            //}else if(thisUnit.location().mapLocation().distanceSquaredTo(GameConstants.startingEnemyLocation)<=thisUnit.visionRange()){
                            //    moveRandom();
                        } else {
                            //System.out.println("USING CACHE");
                            goToEnemyStartUsingBFSCache();
                        }
                    }
                } else {

                    //enemies = gc.senseNearbyUnitsByTeam(thisUnit.location().mapLocation(), thisUnit.visionRange(), Player.enemyTeam);
                    //go to closest enemy
                    if (closestEnemy != null) {
                        if (closestEnemy.location().mapLocation().distanceSquaredTo(thisUnit.location().mapLocation()) <= 10)
                            return;
                        //System.out.println("EY KILLEM");
                        setTargetLoc(closestEnemy.location().mapLocation());
                        takeStepToTarget();
                    } else {
                        moveRandom();
                    }
                }
            }
        }

    }
}
