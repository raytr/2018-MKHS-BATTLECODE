import bc.*;

import java.io.Serializable;
import java.util.*;

class PathingMapLoc implements Serializable{
    public PathingMapLoc parent;
    public boolean onClosedList;

    private int x;
    private int y;
    private Planet planet;
    private Set<PathingMapLoc> neighbors;
    public PathingMapLoc(Planet thePlanet,int X, int Y){
        x = X;
        y = Y;
        planet = thePlanet;
    }

    public MapLocation mapLocation(){
        return new MapLocation(planet,x,y);
    }
    public void resetPathingInfo(){
        onClosedList = false;
        parent=null;
    }

    public Set<PathingMapLoc> getNeighbors(){
        return neighbors;
    }

    public Planet getPlanet() {
        return planet;
    }

    public int getY() {
        return y;
    }

    public int getX() {
        return x;
    }

    public void setX(int X){x=X;}
    public void setY(int Y){y=Y;}

    public void setNeighbors(Set<PathingMapLoc> neighbors) {
        this.neighbors = neighbors;
    }


    @Override
    public boolean equals(Object o) {

        if (o == this) return true;
        if (!(o instanceof PathingMapLoc)) {
            return false;
        }

        PathingMapLoc loc = (PathingMapLoc) o;

        return loc.getX() == x &&
                loc.getY() == y &&
                loc.getPlanet().equals(planet);
    }

    //Idea from effective Java : Item 9
    @Override
    public int hashCode() {
        int result = 17;
        result = 37 * result + x;
        result = 37 * result + y;
        result = 37 * result + planet.hashCode();
        return result;
    }

}
