import bc.*;

//things that will NEVER CHANGE EVER
final class GameConstants {
    public static PlanetMap earthPM;
    public static PlanetMap marsPM;
    public static MapLocation startingEnemyLocation;
    public static MapLocation startingLocation;
    public static Team myTeam;
    public static Team enemyTeam;
    public static int EARTH_MAX_WORKERS;
    public static int EARTH_MAX_HEALERS;
    public static BFSresult BFStoEnemyStart;

    public static BFSmap mapLocSquaresBFSCache;

    public static GameController gc;


    private static PlanetPathLocMap earthPlanetPathLocMap;
    private static PlanetPathLocMap marsPlanetPathLocMap;

    static void init(GameController theGC){
        gc = theGC;

        myTeam = gc.team();
        if(myTeam == Team.Blue){
            enemyTeam = Team.Red;
            System.out.println("blue team");
        }else{
            enemyTeam = Team.Blue;
            System.out.println("red team");
        }

        earthPM = gc.startingMap(Planet.Earth);
        marsPM = gc.startingMap(Planet.Mars);
        EARTH_MAX_WORKERS = (int)(earthPM.getHeight() * earthPM.getWidth())/70;
        EARTH_MAX_HEALERS = EARTH_MAX_WORKERS;
        if(gc.planet() == Planet.Earth){
            VecUnit initUnits = earthPM.getInitial_units();
            for(int i = 0; i < initUnits.size(); i++){
                if(initUnits.get(i).team() == enemyTeam){
                    startingEnemyLocation = initUnits.get(i).location().mapLocation();
                    break;
                }
            }
            for(int i = 0; i < initUnits.size(); i++){
                if(initUnits.get(i).team() == myTeam){
                    startingLocation = initUnits.get(i).location().mapLocation();
                    break;
                }
            }
        }



        earthPlanetPathLocMap = new PlanetPathLocMap(Planet.Earth);
        marsPlanetPathLocMap = new PlanetPathLocMap(Planet.Mars);



        if (gc.planet().equals(Planet.Earth)) BFStoEnemyStart = new BFSresult(earthPlanetPathLocMap,startingEnemyLocation);

        long startTime = System.currentTimeMillis();
        mapLocSquaresBFSCache  = new BFSmap(gc.planet());
        long endTime   = System.currentTimeMillis();
        long totalTime = endTime - startTime;
        System.out.println(totalTime);





    }

    public static PlanetPathLocMap getEarthPlanetPathLocMap(){
        earthPlanetPathLocMap.resetPathingInfo();
        return earthPlanetPathLocMap;
    }

    public static PlanetPathLocMap getMarsPlanetPathLocMap(){
        marsPlanetPathLocMap.resetPathingInfo();
        return marsPlanetPathLocMap;
    }


}
