/**
 * Created by raytr on 1/9/2018.
 */
import bc.*;

class StructureRocket extends StructureBase {
    StructureRocket(GameController gc){
        super(gc);
    }
    private MapLocation loc = new MapLocation(Planet.Mars, 0, 0);
    void loop(){
        if(numTurnsAlive == 0) {
            int x;
            int y;
            do {
                x = (int) Util.randLongInclusive(0, GameConstants.marsPM.getWidth());
                y = (int) Util.randLongInclusive(0, GameConstants.marsPM.getHeight());

            }
            while  (!GameConstants.getMarsPlanetPathLocMap().isLocPassable(x,y));
            loc.setX(x);
            loc.setY(y);
        }
        if (thisUnit.location().isOnMap()) {
            if(thisUnit.location().isOnPlanet(Planet.Mars)){
                if(thisUnit.structureGarrison().size()>0){
                    for(Direction direction : Util.DIRECTIONS){
                        if (gc.canUnload(thisUnit.id(),direction)){
                            gc.unload(thisUnit.id(),direction);
                            break;
                        }
                    }
                }

            } else if(gc.canLaunchRocket(thisUnit.id(), loc)) {
                if(thisUnit.structureGarrison().size() == thisUnit.structureMaxCapacity() || (numTurnsAlive>40&&thisUnit.structureGarrison().size()>1)){
                    gc.launchRocket(thisUnit.id(), loc);
                    System.out.println("GOING TO MARS");
                    RocketLanding landing= new RocketLanding(thisUnit.id(), loc);
                    System.out.println("Destination:" + landing.getDestination().toString());
                }
            }
        }
    }
}
