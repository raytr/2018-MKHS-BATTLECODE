import bc.*;

import java.util.*;
/*
Stores the map using PathingMapLoc objects
 */
class PlanetPathLocMap {
    Planet planet;
    Map<PathingMapLoc,PathingMapLoc> map;

    public Planet getPlanet(){
        return planet;
    }
    public void resetPathingInfo(){
        for (PathingMapLoc pml:map.values()){
            pml.resetPathingInfo();
        }
    }

    public boolean isLocPassable(PathingMapLoc pathingMapLoc){
        return map.containsKey(pathingMapLoc);
    }

    public boolean isLocPassable(int x, int y){
        return map.containsKey(new PathingMapLoc(planet,x,y));
    }

    public PathingMapLoc getPML(PathingMapLoc pathingMapLoc){
        return map.get(pathingMapLoc);
    }

    public PathingMapLoc getPML(int x,int y){
        return map.get(new PathingMapLoc(planet,x,y));
    }

    PlanetPathLocMap(Planet planet){
        PlanetMap pm;
        Map<PathingMapLoc,PathingMapLoc> pathingMap = new HashMap<>();
        if (planet.equals(Planet.Earth)){
            this.planet = Planet.Earth;
            pm = GameConstants.earthPM;
        }else{
            this.planet = Planet.Mars;
            pm = GameConstants.marsPM;
        }



        int[] xMinus = {-1,-1,-1,0,0,1,1,1};
        int[] yMinus = {-1,0,1,-1,1,-1,0,1};

        int width = (int)pm.getWidth();
        int height = (int)pm.getHeight();
        for (int x=0;x<width;x++){
            for (int y=0;y<height;y++){
                MapLocation mapLocation = new MapLocation(planet,x,y);
                if (pm.isPassableTerrainAt(mapLocation) == 1){
                    pathingMap.put(new PathingMapLoc(planet,x,y),new PathingMapLoc(planet,x,y));
                    // System.out.println(pathingMap.containsKey(mapLocation));
                }
            }
        }

        //set the neighbors
        for (PathingMapLoc pml : pathingMap.values()){
            Set<PathingMapLoc> finalNeighbors = new HashSet();
            int x= pml.getX();
            int y = pml.getY();
            for (int i=0;i<8;i++){
                PathingMapLoc newXYmapLocation = new PathingMapLoc(planet,x - xMinus[i],y - yMinus[i]);
                if (pathingMap.containsKey(newXYmapLocation)){
                    PathingMapLoc thisPathingMapLoc = pathingMap.get(newXYmapLocation);
                    finalNeighbors.add(thisPathingMapLoc);
                }
            }
            pml.setNeighbors(finalNeighbors);
        }
        map = pathingMap;
    }

}
