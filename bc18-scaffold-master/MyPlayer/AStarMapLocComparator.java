import java.util.Comparator;

/**
 * Created by raytr on 1/10/2018.
 */
class AStarMapLocComparator implements Comparator<AStarMapLoc> {
    @Override
    public int compare(AStarMapLoc a, AStarMapLoc b){
        //if the f values tie, look at lower h
        if (a.f == b.f){
            if (a.h == b.h){
                //complete tie
                return 0;
            }
            else if (a.h > b.h) return 1;
            else return -1;
        }
        else if (a.f > b.f) return 1;
        else return -1;
    }
}
