import bc.*;

import java.io.*;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

//things that will NEVER CHANGE EVER
final class GameConstants {
    public static PlanetMap earthPM;
    public static PlanetMap marsPM;
    public static MapLocation startingEnemyLocation;
    public static MapLocation startingLocation;
    public static Team myTeam;
    public static Team enemyTeam;
    public static int EARTH_MAX_WORKERS;
    public static int EARTH_MAX_HEALERS;
    public static HashMap<PathingMapLoc,PathingMapLoc> BFStoEnemyStart;
    public static GameController gc;

    private static HashMap<PathingMapLoc,PathingMapLoc> earthPathingMap = new HashMap<>();
    private static HashMap<PathingMapLoc,PathingMapLoc> marsPathingMap = new HashMap<>();

    static void init(GameController theGC){
        gc = theGC;

        myTeam = gc.team();
        if(myTeam == Team.Blue){
            enemyTeam = Team.Red;
            System.out.println("blue team");
        }else{
            enemyTeam = Team.Blue;
            System.out.println("red team");
        }

        earthPM = gc.startingMap(Planet.Earth);
        marsPM = gc.startingMap(Planet.Mars);
        EARTH_MAX_WORKERS = (int)(earthPM.getHeight() * earthPM.getWidth())/70;
        EARTH_MAX_HEALERS = EARTH_MAX_WORKERS;
        if(gc.planet() == Planet.Earth){
            VecUnit initUnits = earthPM.getInitial_units();
            for(int i = 0; i < initUnits.size(); i++){
                if(initUnits.get(i).team() == enemyTeam){
                    startingEnemyLocation = initUnits.get(i).location().mapLocation();
                    break;
                }
            }
            for(int i = 0; i < initUnits.size(); i++){
                if(initUnits.get(i).team() == myTeam){
                    startingLocation = initUnits.get(i).location().mapLocation();
                    break;
                }
            }
        }


        long startTime = System.currentTimeMillis();


        initPathingMap(Planet.Earth);
        initPathingMap(Planet.Mars);
        long endTime   = System.currentTimeMillis();
        long totalTime = endTime - startTime;
        //System.out.println(totalTime);


        if (gc.planet().equals(Planet.Earth)) BFStoEnemyStart = BotBase.runFullBFS(startingEnemyLocation);

    }

    public static Map<PathingMapLoc,PathingMapLoc> getEarthPathingMap(){
        for (PathingMapLoc pml:earthPathingMap.values()){
            pml.resetPathingInfo();
        }
        return earthPathingMap;
    }

    public static Map<PathingMapLoc,PathingMapLoc> getMarsPathingMap(){
        for (PathingMapLoc pml:marsPathingMap.values()){
            pml.resetPathingInfo();
        }
        return marsPathingMap;
    }



    private static void initPathingMap(Planet planet){
        PlanetMap pm;
        Map<PathingMapLoc,PathingMapLoc> pathingMap;
        if (planet.equals(Planet.Earth)){
            pathingMap = earthPathingMap;
            pm = earthPM;
        }else{
            pathingMap = marsPathingMap;
            pm = marsPM;
        }



        int[] xMinus = {-1,-1,-1,0,0,1,1,1};
        int[] yMinus = {-1,0,1,-1,1,-1,0,1};

        int width = (int)pm.getWidth();
        int height = (int)pm.getHeight();
        for (int x=0;x<width;x++){
            for (int y=0;y<height;y++){
                MapLocation mapLocation = new MapLocation(planet,x,y);
                if (pm.isPassableTerrainAt(mapLocation) == 1){
                    pathingMap.put(new PathingMapLoc(planet,x,y),new PathingMapLoc(planet,x,y));
                    // System.out.println(pathingMap.containsKey(mapLocation));
                }
            }
        }

        //set the neighbors
        for (PathingMapLoc pml : pathingMap.values()){
            Set<PathingMapLoc> finalNeighbors = new HashSet();
            int x= pml.getX();
            int y = pml.getY();
            for (int i=0;i<8;i++){
                PathingMapLoc newXYmapLocation = new PathingMapLoc(planet,x - xMinus[i],y - yMinus[i]);
                if (pathingMap.containsKey(newXYmapLocation)){
                    PathingMapLoc thisPathingMapLoc = pathingMap.get(newXYmapLocation);
                    finalNeighbors.add(thisPathingMapLoc);
                }
            }
            pml.setNeighbors(finalNeighbors);
        }
    }
}
